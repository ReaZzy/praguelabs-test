export enum VehicleType {
  INTERGRATED = 'Intergrated',
  BUILTIN = 'BuiltIn',
  ALCOVE = 'Alcove',
  CAMPERVAN = 'Campervan',
}
