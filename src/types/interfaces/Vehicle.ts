import { VehicleType } from 'types/enums/VehicleType';

export interface Vehicle {
  location: string;
  instantBookable: boolean;
  name: string;
  passengersCapacity: number;
  sleepCapacity: number;
  price: number;
  vehicleType: VehicleType;
  toilet: boolean;
  shower: boolean;
  pictures: Array<string>;
}

export interface VehicleResponse {
  count: number;
  items: Array<Vehicle>;
}
