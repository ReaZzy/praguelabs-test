import React, { InputHTMLAttributes } from 'react';
import styled from 'styled-components';

const CheckboxContainer = styled.label`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 0.2rem;
`;

const Icon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 2px;
`;

const HiddenCheckbox = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const StyledCheckbox = styled.div<{ checked: boolean }>`
  display: inline-block;
  width: 16px;
  height: 16px;
  background: ${(props) =>
    props.checked ? props.theme.green : props.theme.grey};
  border-radius: 3px;
  transition: all 150ms;

  ${HiddenCheckbox}:focus + & {
    box-shadow: 0 0 0 3px ${(props) => props.theme.greenDark};
  }

  ${Icon} {
    visibility: ${(props) => (props.checked ? 'visible' : 'hidden')};
  }
`;

interface CheckboxProps extends InputHTMLAttributes<HTMLInputElement> {
  checked: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  labelText?: string;
  className?: string;
}

export const Checkbox: React.FC<CheckboxProps> = React.memo(
  ({ className, checked = false, labelText, ...props }) => {
    return (
      <CheckboxContainer className={className}>
        <HiddenCheckbox type={'checkbox'} checked={checked} {...props} />
        <StyledCheckbox checked={checked}>
          <Icon viewBox="0 0 24 24">
            <polyline points="20 6 9 17 4 12" />
          </Icon>
        </StyledCheckbox>
        {labelText || ''}
      </CheckboxContainer>
    );
  }
);

Checkbox.displayName = 'Checkbox';
