import React from 'react';
import { Card, theme, Title } from 'Components/LayoutComponents';
import { Vehicle } from 'Types/interfaces/Vehicle';
import Image from 'next/image';
import styled from 'styled-components';
import Toilet from 'Utils/svg/toilet.svg';
import Instant from 'Utils/svg/instant.svg';
import Shower from 'Utils/svg/shower.svg';

interface ItemCardProps {
  vehicle: Vehicle;
}

const loader = ({ src }: { src: string }) => {
  return `${src}`;
};

export const ItemCard: React.FC<ItemCardProps> = React.memo(
  ({
    vehicle: {
      vehicleType,
      shower,
      instantBookable,
      name,
      passengersCapacity,
      sleepCapacity,
      price,
      toilet,
      pictures,
      location,
    },
  }) => {
    return (
      <Card data-testid={'item-card'}>
        <ImageWrapper>
          <Image
            unoptimized
            loader={loader}
            src={pictures[0]}
            layout={'fill'}
            alt={`${name}-image`}
          />
        </ImageWrapper>

        <CardContent>
          <div>
            <Title>{name}</Title>
            <SmallText>
              {vehicleType}, {location}
            </SmallText>
          </div>
          <Content>
            <p>Passengers capacity: {passengersCapacity}</p>
            <p>Sleep capacity: {sleepCapacity}</p>
            <p>Price: {price}</p>
          </Content>
          <ProsWrapper>
            <ProsText>Pros:</ProsText>
            <ProsFlex>
              {shower && (
                <Shower style={{ fill: theme.green, width: '2rem' }} />
              )}
              {instantBookable && (
                <Instant style={{ fill: theme.green, width: '2rem' }} />
              )}
              {toilet && (
                <Toilet style={{ fill: theme.green, width: '1.5rem' }} />
              )}
            </ProsFlex>
          </ProsWrapper>
        </CardContent>
      </Card>
    );
  }
);

const ImageWrapper = styled.div`
  height: 15rem;
  position: relative;
`;

const CardContent = styled.div`
  padding: 1rem;
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 0.4rem;
`;
const ProsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.2rem;
`;
const Content = styled(ProsWrapper)`
  text-align: start;
`;
const ProsFlex = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.2rem;
  align-items: center;
`;
const ProsText = styled.p`
  font-size: 1rem;
  text-align: left;
  font-weight: bold;
`;
const SmallText = styled.p`
  color: ${(props) => props.theme.gray};
  font-size: 1rem;
  font-style: italic;
`;

ItemCard.displayName = 'ItemCard';
