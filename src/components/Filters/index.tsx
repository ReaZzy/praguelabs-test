import React, { useCallback, useMemo, useState } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { VehicleType } from 'Types/enums/VehicleType';
import { UseFiltersResponse } from 'Hooks/useFilters/useFilters';
import { Vehicle } from 'Types/interfaces/Vehicle';
import { useDebouncedCallback } from 'use-debounce';
import { Button, Flex, SearchInput, theme } from 'Components/LayoutComponents';
import Select from 'react-select';
import styled from 'styled-components';
import { Checkbox } from 'Components/Checkbox';

interface FiltersProps<T = unknown> {
  filters: UseFiltersResponse<T>['filters'];
  handleChange: UseFiltersResponse<T>['handleChange'];
  filterData: UseFiltersResponse<T>['filterData'];
  clearFilters: UseFiltersResponse<T>['clearFilters'];
}

const options: Array<{ value: VehicleType; label: VehicleType }> =
  Object.values(VehicleType).map((type) => ({ value: type, label: type }));

export const Filters: React.FC<FiltersProps<Vehicle>> = React.memo(
  ({ handleChange, filters, filterData, clearFilters }) => {
    const [range, setRange] = useState<Array<number>>([100, 10000]);
    const debounced = useDebouncedCallback((debouncedRange: Array<number>) => {
      if (
        debouncedRange[0] >= 100 &&
        debouncedRange[0] <= 10000 &&
        debouncedRange[1] >= 100 &&
        debouncedRange[1] <= 10000 &&
        debouncedRange[0] <= debouncedRange[1]
      ) {
        return filterData({
          price: debouncedRange,
        });
      }
    }, 300);
    const handleSelect = (
      items: Array<{ value: VehicleType; label: VehicleType }>
    ) => {
      const vehicleType = items.map((type) => type.value);
      filterData({
        vehicleType,
      });
    };

    const handleClear = () => {
      setRange([100, 10000]);
      clearFilters();
    };

    const searchTermInput = useMemo(
      () => (
        <SearchInput
          type="search"
          onChange={handleChange}
          name={'name'}
          placeholder={'Search...'}
          value={filters?.name || ''}
          autoFocus
        />
      ),
      [handleChange, filters.name]
    );

    const rangeInput = useCallback(
      (value: number, onChange: (e: string) => void) => (
        <SearchInput
          type={'number'}
          onChange={(e) => {
            onChange(e.target.value);
          }}
          value={value}
        />
      ),
      []
    );

    return (
      <FlexColumn>
        {searchTermInput}
        <div>
          <Slider
            prefixCls="rc-slider"
            range
            min={100}
            max={10000}
            draggableTrack
            allowCross
            onChange={(range) => {
              setRange(range as Array<number>);
              debounced(range as Array<number>);
            }}
            trackStyle={[{ backgroundColor: theme.green }]}
            railStyle={{ backgroundColor: theme.lightGrey }}
            value={range}
          />
          <Flex>
            {rangeInput(range[0], (e) => {
              setRange((prevState) => [Number(e), prevState[1]]);
              debounced([Number(e), range[1]]);
            })}
            {rangeInput(range[1], (e) => {
              setRange((prevState) => [prevState[0], Number(e)]);
              debounced([range[0], Number(e)]);
            })}
          </Flex>
        </div>

        <Select
          instanceId={'react-select'}
          styles={{
            multiValue: (styles) => ({
              ...styles,
              backgroundColor: theme.green,
            }),
            multiValueLabel: (styles) => ({
              ...styles,
              color: theme.lightGrey,
            }),
            multiValueRemove: (styles) => ({
              ...styles,
              color: theme.lightGrey,
              ':hover': {
                color: theme.grey,
              },
            }),
          }}
          value={
            filters?.vehicleType?.map((type: VehicleType) => ({
              value: type,
              label: type,
            })) ?? []
          }
          options={options}
          onChange={handleSelect as any}
          isMulti
        />
        <Checkbox
          checked={filters?.toilet}
          onChange={handleChange}
          name={'toilet'}
          labelText={'Toilet'}
        />
        <Checkbox
          checked={filters?.instantBookable}
          onChange={handleChange}
          name={'instantBookable'}
          labelText={'Instant bookable'}
        />
        <Checkbox
          checked={filters?.shower}
          onChange={handleChange}
          name={'shower'}
          labelText={'Shower'}
        />
        <Button onClick={handleClear}>Clear</Button>
      </FlexColumn>
    );
  }
);

const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.6rem;
  height: 100%;
`;

Filters.displayName = 'Filters';
