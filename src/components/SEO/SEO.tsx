import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

interface SEOProps {
  description?: string;
  title?: string;
  image?: string;
  url?: string;
}

export const SEO: React.FC<SEOProps> = React.memo(
  ({ title, description, url, image }) => {
    const router = useRouter();
    return (
      <Head>
        <title>{title || 'PragueLabs'}</title>
        <meta
          property="description"
          content={description || 'PragueLabs test task'}
        />
        <meta property="og:title" content={title || 'PragueLabs'} />
        <meta
          property="og:description"
          content={description || 'PragueLabs test task'}
        />
        <meta
          property="og:url"
          content={url || `http://localhost:3000${router?.asPath || ''}`}
        />
        <meta
          property="og:image"
          content={
            image ||
            'https://media-exp1.licdn.com/dms/image/C4D0BAQH91JmnNfAjiw/company-logo_200_200/0/1519874754363?e=2147483647&v=beta&t=g_-ECUmlzL8d4_tToZ0YHexX0mfwttTgXAB99SU_pYc'
          }
        />
      </Head>
    );
  }
);

SEO.displayName = 'SEO';
