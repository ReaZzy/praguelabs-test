import styled from 'styled-components';

export const theme = {
  black: '#232121',
  green: '#59bb31',
  greenDark: '#56a92d',
  grey: '#c7c7c7',
  lightGrey: '#f8f8f8',
};

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.2rem;
`;

export const Heading = styled.h1`
  margin: 0;
  color: ${(props) => props.theme.black};
`;

export const Title = styled.h2`
  margin: 0;
  color: ${(props) => props.theme.black};
  font-size: 1.2rem;
`;

export const Button = styled.button`
  outline: none;
  margin: 0;
  border: none;
  color: ${(props) => props.theme.black};
  background-color: ${(props) => props.theme.lightGrey};
  padding: 1rem 2rem;
  border-radius: 0.5rem;
  transition: 0.2s;
  cursor: pointer;
  &:hover {
    color: ${(props) => props.theme.lightGrey};
    background-color: ${(props) => props.theme.green};
  }
`;

export const Card = styled.div`
  width: 100%;
  overflow: hidden;
  border-radius: 1rem;
  min-height: 10rem;
  background-color: ${(props) => props.theme.lightGrey};

  @media (min-width: 1024px) {
    width: 22rem;
  }
`;

export const SearchInput = styled.input`
  padding: 0.5rem;
  border-radius: 0.5rem;
  width: 100%;
  outline: none;
  border: none;
  box-shadow: 0 0 10px -6px ${(props) => props.theme.black};
  -webkit-box-shadow: 0 0 10px -6px ${(props) => props.theme.black};
  -moz-box-shadow: 0 0 10px -6px ${(props) => props.theme.black};
  &:focus {
    outline: 1px solid ${(props) => props.theme.green};
    border: 1px solid ${(props) => props.theme.green};
  }
`;
