import { ChangeEvent, useCallback, useEffect, useState } from 'react';

export type QueryType<T = unknown> = Partial<Record<keyof T, any>>;

export type UseFiltersResponse<T> = {
  filteredData: Array<T>;
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
  filters: QueryType<T>;
  filterData: (filter: Partial<Record<keyof T, any>>) => void;
  clearFilters: () => void;
};

export const useFilters = <T = {}>(
  initialData: Array<T>,
  queries: QueryType<T>
): UseFiltersResponse<T> => {
  const [filteredData, setFilteredData] = useState(initialData);
  const [filters, setFilters] = useState(queries);

  const handleChange: UseFiltersResponse<T>['handleChange'] = useCallback(
    (e) => {
      const newFilters = {
        ...filters,
        [e.target.name]:
          e.target.type === 'checkbox' ? e.target.checked : e.target.value,
      };
      setFilters(newFilters);
    },
    [filters]
  );

  const filterData: UseFiltersResponse<T>['filterData'] = useCallback(
    (filter) => {
      const newFilters = { ...filters, ...filter };
      setFilters(newFilters);
    },
    [filters]
  );

  const clearFilters: UseFiltersResponse<T>['clearFilters'] =
    useCallback(() => {
      setFilters({});
    }, []);

  useEffect(() => {
    setFilteredData(
      initialData.filter((item) => {
        const check: Array<boolean> = [];
        (Object.keys(item) as Array<keyof T>).map((key) => {
          if (!filters[key]) return check.push(true);
          if (
            typeof filters[key] === 'object' &&
            filters[key].length > 0 &&
            (filters[key] as any)?.every((item: any) => Number.isInteger(item))
          ) {
            return check.push(
              item[key] >= filters[key][0] && item[key] <= filters[key][1]
            );
          }
          if (typeof filters[key] === 'object') {
            return (
              filters[key].length === 0 ||
              check.push(
                filters[key].some((query: string) =>
                  (item[key] as unknown as string)
                    ?.toLowerCase()
                    .includes(query?.toLowerCase())
                )
              )
            );
          }
          if (typeof item[key] === 'string') {
            return (
              filters[key].replace(/ /g, '').length === 0 ||
              check.push(
                (item[key] as unknown as string)
                  .toLowerCase()
                  ?.includes(
                    typeof filters[key] === 'string'
                      ? (filters[key] as string)?.toLowerCase()
                      : ''
                  )
              )
            );
          }
          check.push(item[key] === filters[key]);
        });
        return check.every((b) => b);
      })
    );
  }, [filters, initialData]);

  return {
    filteredData,
    filters,
    filterData,
    handleChange,
    clearFilters,
  };
};
