import React from 'react';
import { useFilters } from 'Hooks/useFilters/useFilters';
import { Vehicle } from 'Types/interfaces/Vehicle';
import { VehicleType } from 'Types/enums/VehicleType';
import { fireEvent, render, screen } from '@testing-library/react';

const initialData = [
  {
    location: 'Zbýšov',
    instantBookable: false,
    name: 'A 699VB',
    passengersCapacity: 7,
    sleepCapacity: 7,
    price: 2500,
    toilet: true,
    shower: true,
    vehicleType: VehicleType.BUILTIN,
    pictures: [
      'https://d35xwkx70uaomf.cloudfront.net/017bc56a-8266-4c59-a6d3-212224e77740.jpg',
    ],
  },
  {
    location: 'Trutnov',
    instantBookable: false,
    name: 'I 68',
    passengersCapacity: 4,
    sleepCapacity: 5,
    price: 2600,
    toilet: false,
    shower: true,
    vehicleType: VehicleType.CAMPERVAN,
    pictures: [
      'https://d35xwkx70uaomf.cloudfront.net/017bc53e-a1d5-40b5-abb6-8deb589bc22f.jpg',
    ],
  },
];

const TestComponent = () => {
  const { filterData, clearFilters, filteredData, handleChange } =
    useFilters<Vehicle>(initialData, {});

  return (
    <>
      {filteredData.map((item, index) => (
        <div key={index} data-testid={'filtered-item'}>
          <span data-testid={'filtered-item-name'}>{item.name}</span>
          <span data-testid={'filtered-item-price'}>{item.price}</span>{' '}
          <span data-testid={'filtered-item-toilet'}>
            {item.toilet && 'toilet'}
          </span>
          <span data-testid={'filtered-item-shower'}>
            {item.shower && 'shower'}
          </span>
        </div>
      ))}
      <button onClick={clearFilters} data-testid={'filtered-clear-filters'}>
        clear filters
      </button>
      <button
        data-testid={'filtered-two-filters'}
        onClick={() => {
          filterData({
            vehicleType: [VehicleType.BUILTIN, VehicleType.CAMPERVAN],
          });
        }}
      >
        set 2 types
      </button>
      <button
        data-testid={'filtered-range'}
        onClick={() => {
          filterData({
            price: [0, 2500],
          });
        }}
      >
        set range
      </button>
      <button
        data-testid={'filtered-one-filter'}
        onClick={() => {
          filterData({
            vehicleType: VehicleType.BUILTIN,
          });
        }}
      >
        set 1 types
      </button>
      <input
        onChange={handleChange}
        name={'name'}
        data-testid={'filtered-name'}
      />
      <input
        onChange={handleChange}
        type={'checkbox'}
        name={'toilet'}
        data-testid={'filtered-toilet'}
      />
      <input
        onChange={handleChange}
        name={'shower'}
        type={'checkbox'}
        data-testid={'filtered-shower'}
      />
    </>
  );
};

TestComponent.displayName = 'TestComponent';

describe('useFilters', () => {
  beforeEach(() => {
    render(<TestComponent />);
  });
  it('Should return initial data without filters', () => {
    expect(screen.getAllByTestId('filtered-item')).toHaveLength(2);
  });
  it('Should hide all elements if doesnt match anything', () => {
    fireEvent.change(screen.getByTestId('filtered-name'), {
      target: { value: '123' },
    });
    expect(screen.queryByTestId('filtered-item')).toBeNull();
  });
  it('Should show all elements if match', () => {
    fireEvent.change(screen.getByTestId('filtered-name'), {
      target: { value: 'A 699VB' },
    });
    expect(screen.getByTestId('filtered-item-name').textContent).toEqual(
      'A 699VB'
    );
  });
  it('Should show elements with couple of filters', () => {
    fireEvent.click(screen.getByTestId('filtered-shower'));
    expect(screen.getAllByTestId('filtered-item-name')).toHaveLength(2);
    fireEvent.click(screen.getByTestId('filtered-toilet'));
    expect(screen.getAllByTestId('filtered-item-name')).toHaveLength(1);
    fireEvent.change(screen.getByTestId('filtered-name'), {
      target: { value: 'A 699VB' },
    });
    expect(screen.getAllByTestId('filtered-item-name')).toHaveLength(1);
  });
  it('Clear filters should work fine', () => {
    fireEvent.click(screen.getByTestId('filtered-toilet'));
    expect(screen.getAllByTestId('filtered-item-name')).toHaveLength(1);
    fireEvent.click(screen.getByTestId('filtered-clear-filters'));
    expect(screen.getAllByTestId('filtered-item-name')).toHaveLength(2);
  });
  it('If we put array to a query should find some', () => {
    fireEvent.click(screen.getByTestId('filtered-one-filter'));
    expect(screen.getAllByTestId('filtered-item')).toHaveLength(1);
    fireEvent.click(screen.getByTestId('filtered-two-filters'));
    expect(screen.getAllByTestId('filtered-item')).toHaveLength(2);
  });
  it('If we put number array should find in array range', () => {
    fireEvent.click(screen.getByTestId('filtered-range'));
    expect(screen.getAllByTestId('filtered-item')).toHaveLength(1);
    expect(screen.getByTestId('filtered-item-price').textContent).toEqual(
      '2500'
    );
  });
});
