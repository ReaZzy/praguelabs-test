const nextJest = require('next/jest');

const createJestConfig = nextJest({
  dir: './',
});

const customJestConfig = {
  moduleDirectories: ['node_modules', '<rootDir>/'],
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    '^Components(.*)$': '<rootDir>/src/components/$1',
    '^Hooks(.*)$': '<rootDir>/src/hooks/$1',
    '^Types(.*)$': '<rootDir>/src/types/$1',
    '^Utils(.*)$': '<rootDir>/src/__mocks__/svg.js',
    '\\.svg$': '<rootDir>/src/__mocks__/svg.js',
  },
};

module.exports = createJestConfig(customJestConfig);
