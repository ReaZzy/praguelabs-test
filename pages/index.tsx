import React, { useEffect, useMemo, useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { VehicleResponse } from 'Types/interfaces/Vehicle';
import { Heading, theme, Button, Title } from 'Components/LayoutComponents';
import { Filters } from 'Components/Filters';
import { useFilters } from 'Hooks/useFilters/useFilters';
import { ItemCard } from 'Components/ItemCard';
import { SEO } from 'Components/SEO/SEO';

interface HomeProps {
  data: VehicleResponse;
}

const Home: React.FC<HomeProps> = ({ data }) => {
  const { filteredData, handleChange, filters, filterData, clearFilters } =
    useFilters<HomeProps['data']['items'][number]>(data?.items, {});
  const [showCount, setShowCount] = useState<number>(10);
  const handleShowMore = () => {
    setShowCount((prevState) => prevState + 10);
  };

  useEffect(() => {
    setShowCount(10);
  }, [filters]);

  const itemsList = useMemo(() => {
    if (filteredData.length > 0) {
      return filteredData
        .slice(0, showCount)
        .map((vehicle, index) => <ItemCard key={index} vehicle={vehicle} />);
    }
    return <div>Can&#39;t find any vehicle with these filters</div>;
  }, [filteredData, showCount]);

  return (
    <ThemeProvider theme={theme}>
      <SEO />
      <PageWrapper data-testid="HomePage">
        <Heading data-testid="HomePageHeading">Prague labs test task</Heading>
        <ContentLayout>
          <FiltersWrapper>
            <Title>Filters</Title>
            <Filters
              handleChange={handleChange}
              filters={filters}
              filterData={filterData}
              clearFilters={clearFilters}
            />
          </FiltersWrapper>
          <MainGap>
            <List>{itemsList}</List>
            {filteredData.length > showCount && (
              <Button onClick={handleShowMore} data-testid={'show-more-button'}>
                Show more
              </Button>
            )}
          </MainGap>
        </ContentLayout>
      </PageWrapper>
    </ThemeProvider>
  );
};

const PageWrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  text-align: center;
`;

const MainGap = styled.main`
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;

const List = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  justify-content: flex-start;
  flex-grow: 1;
  gap: 1rem;

  @media (min-width: 764px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: 1024px) {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    gap: 2rem;
  }
`;

const ContentLayout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  gap: 1rem;
  margin: 1rem;

  @media (min-width: 1024px) {
    flex-direction: row;
    gap: 2rem;
  }
`;

const FiltersWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 0.5rem;

  @media (min-width: 1024px) {
    padding: 0 1rem;
    flex-basis: 25rem;
    min-width: 25rem;
    flex-grow: 0;
  }
`;

export const getServerSideProps = async () => {
  const res = await fetch(`http://localhost:3000/api/data`);
  const data: HomeProps['data'] = await res.json();
  return { props: { data } };
};

export default Home;
