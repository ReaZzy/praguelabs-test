import Home from './index';
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import { Vehicle } from 'Types/interfaces/Vehicle';
import { VehicleType } from 'Types/enums/VehicleType';

const item = {
  location: 'Praha',
  instantBookable: true,
  name: 'Matrix Axess 600 SL',
  passengersCapacity: 4,
  sleepCapacity: 5,
  price: 2400,
  toilet: true,
  shower: true,
  vehicleType: VehicleType.BUILTIN,
  pictures: [
    'https://d35xwkx70uaomf.cloudfront.net/01797fed-ef72-4df7-b08a-f48c67b0fd49.jpg',
  ],
};

describe('Home page', () => {
  let data: { count: number; items: Array<Vehicle> };
  beforeEach(() => {
    data = {
      count: 2,
      items: [
        item,
        item,
        item,
        item,
        item,
        item,
        item,
        item,
        item,
        item,
        item,
        item,
      ],
    };
  });

  it('Should render Home page without errors', () => {
    render(<Home data={data} />);
    expect(screen.getByTestId('HomePage')).toBeDefined();
  });
  it('Should render heading', () => {
    render(<Home data={data} />);
    expect(screen.getByTestId('HomePageHeading').textContent).toEqual(
      'Prague labs test task'
    );
  });
  it('Should render only 10 elements', () => {
    render(<Home data={data} />);
    expect(screen.getAllByTestId('item-card')).toHaveLength(10);
  });
  it('Should 10 more elements on click', () => {
    render(<Home data={data} />);
    expect(screen.getAllByTestId('item-card')).toHaveLength(10);
    fireEvent.click(screen.getByTestId('show-more-button'));
    expect(screen.getAllByTestId('item-card')).toHaveLength(12);
  });
});
